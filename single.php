<?php
include '.include.php';
$accounts = get_accounts(array('id'));
$accounts_by_date = get_accounts();
htmlentities_array_array($accounts);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php echo $htmlTitle; ?></title>
<link rel="stylesheet" href="<?php echo $cssInclude; ?>" />
</head>
<body>

<main class="container-fluid">
<?php include 'navigation.html'; ?>

<h1 style="color: #ed3bbe;">Single Transactions</h1>
<a href="edit-single.php">Add New Single Transaction</a>
<figure>
<table role="grid">
<thead>
<tr>
<th>Name</th>
<th>Amount</th>
<th>From</th>
<th>To</th>
<th>Date</th>
</tr>
</thead>
<tbody>
<?php
$curDate = get_earliest_account_date($accounts_by_date);
foreach (get_single() as $rec) {
    $fromToTag = 'ins';
    $dateTag = 'ins';
    $recDate = date_create($rec['date']);
    if ($recDate < $curDate) $dateTag = 'del';
    if ($rec['from'] == $rec['to']) $fromToTag = 'mark';
    htmlentities_array($rec);
    $fromName = $accounts[$rec['from']]['name'];
    $toName = $accounts[$rec['to']]['name'];
    print("
<tr>
<td><a href=\"edit-single.php?id=${rec['id']}\">${rec['name']}</a></td>
<td>\$ ${rec['amount']}</td>
<td><$fromToTag>${fromName}</$fromToTag></td>
<td><$fromToTag>${toName}</$fromToTag></td>
<td><$dateTag>${rec['date']}</$dateTag></td>
</tr>
");
}
?>
</tbody>
</table>
</figure>
</main>

</body>
</html>
