<?php
include '.include.php';
$accounts = get_accounts(array('id'));
$accounts_by_date = get_accounts();
htmlentities_array_array($accounts);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php echo $htmlTitle; ?></title>
<link rel="stylesheet" href="<?php echo $cssInclude; ?>" />
</head>
<body>

<main class="container-fluid">
<?php include 'navigation.html'; ?>

<h1 style="color: #ed3bbe;">Recurring Transactions</h1>
<a href="edit-recurring.php">Add New Recurring Transaction</a>
<figure>
<table role="grid">
<thead>
<tr>
<th>Name</th>
<th>Amount</th>
<th>Every</th>
<th>From</th>
<th>To</th>
<th>Start Date</th>
<th>End Date</th>
</tr>
</thead>
<tbody>
<?php
$curDate = get_earliest_account_date($accounts_by_date);
foreach (get_recurring() as $rec) {
    $fromToTag = 'ins';
    $startDateTag = 'ins';
    $endDateTag = 'ins';
    $startDate = date_create($rec['startDate']);
    $endDate = date_create($rec['endDate']);
    if ($endDate < $curDate) $endDateTag = 'del';
    if ($endDate <= $startDate) $endDateTag = 'mark';
    if ($rec['endDate'] == false) $endDateTag = 'ins';
    if ($rec['from'] == $rec['to']) $fromToTag = 'mark';
    htmlentities_array($rec);
    $fromName = $accounts[$rec['from']]['name'];
    $toName = $accounts[$rec['to']]['name'];
    print("
<tr>
<td><a href=\"edit-recurring.php?id=${rec['id']}\">${rec['name']}</a></td>
<td>\$ ${rec['amount']}</td>
<td>Every ${rec['every_x']} ${rec['every']}</td>
<td><$fromToTag>${fromName}</$fromToTag></td>
<td><$fromToTag>${toName}</$fromToTag></td>
<td><$startDateTag>${rec['startDate']}</$startDateTag></td>
<td><$endDateTag>${rec['endDate']}</$endDateTag></td>
</tr>
");
}
?>
</tbody>
</table>
</figure>
</main>

</body>
</html>
