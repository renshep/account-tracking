<?php
include '.include.php';
$accounts = get_accounts();
$account_by_id = get_accounts(array('id'));
$single = get_single();
$recurring = get_recurring();
bcscale(2);

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php echo $htmlTitle; ?></title>
<link rel="stylesheet" href="<?php echo $cssInclude; ?>" />
</head>
<body>

<main class="container-fluid">
<?php include 'navigation.html'; ?>

<h1 style="color: #ed3bbe;">Balance Report</h1>
<?php
$startDate = get_earliest_account_date($accounts);
$endDate = date_modify(clone $startDate,$reportLength);
$startDateStr = htmlentities(date_to_str($startDate));
$endDateStr = htmlentities(date_to_str($endDate));
print("<pre>
Report Start: ${startDateStr}
Report   End: ${endDateStr}
</pre>
");
// generate every transaction
$trans_by_date = array(); // date, name, amount, from, to
$trans_by_account_by_date = array();
// calculate account balances for each day for each transaction
$running_balances = array();
$account_balances_by_day = array();
$account_mins = array();
$account_maxs = array();
foreach ($accounts as $account) {
    $accountId = $account['id'];
    $account_mins[$accountId] = $account['balance'];
    $account_maxs[$accountId] = $account['balance'];
    $running_balances[$accountId] = $account['balance'];
    $newEntry = array(
        'date' => $account['startDate'],
        'name' => 'Balance',
        'change' => '0.00',
        'balance' => $account['balance'],
    );
    add_to_indexed_indexed_array($account_balances_by_day,$accountId,$account['startDate'],$newEntry);
}

// generate all single transactions
foreach ($single as $trans) {
    $transDate = date_create($trans['date']);
    if ($transDate >= $startDate and $transDate <= $endDate) {
        $newTransaction = new_transaction($trans['date'],$trans['name'],$trans['amount'],$trans['from'],$trans['to']);
        add_to_indexed_array($trans_by_date,$trans['date'],$newTransaction);
        add_to_indexed_indexed_array($trans_by_account_by_date,$trans['to'],$trans['date'],$newTransaction);
        add_to_indexed_indexed_array($trans_by_account_by_date,$trans['from'],$trans['date'],$newTransaction);
    }
}
// generate all recurring transactions
foreach ($recurring as $trans) {
    $transStartDate = date_create($trans['startDate']);
    $transEndDate = $trans['endDate'];
    if ($transEndDate == false) $transEndDate = '9999-12-31';
    $transEndDate = date_create($transEndDate);
    $transRate = "+${trans['every_x']} ${trans['every']}";
    $transDate = clone $transStartDate;
    while ($transDate <= $endDate and $transDate <= $transEndDate) {
        if ($transDate >= $startDate) {
            $transDateStr = date_to_str($transDate);
            $newTransaction = new_transaction($transDateStr,$trans['name'],$trans['amount'],$trans['from'],$trans['to']);
            add_to_indexed_array($trans_by_date,$transDateStr,$newTransaction);
            add_to_indexed_indexed_array($trans_by_account_by_date,$trans['to'],$transDateStr,$newTransaction);
            add_to_indexed_indexed_array($trans_by_account_by_date,$trans['from'],$transDateStr,$newTransaction);
        }
        date_modify($transDate,$transRate);
    }
}
ksort($trans_by_date);
// calculate all balances
foreach ($trans_by_account_by_date as $accountId => $trans) {
    ksort($trans);
    foreach ($trans as $transDate => $transArray) {
        $deposits = array();
        $withdrawals = array();
        foreach ($transArray as $curTrans) {
            if ($accountId == $curTrans['from']) {
                // its a withdrawal
                $withdrawals[] = $curTrans;
            } else if ($accountId == $curTrans['to']) {
                // it's a deposit
                $deposits[] = $curTrans;
            }
        } 
        foreach ($deposits as $curTrans) {
            $newBalance = $running_balances[$accountId];
            $change = '0.00';
            if ($accountId == $curTrans['from']) {
                // its a withdrawal
                $newBalance = bcsub($newBalance,$curTrans['amount']);
                $change = '-' . $curTrans['amount'];
            } else if ($accountId == $curTrans['to']) {
                // it's a deposit
                $newBalance = bcadd($newBalance,$curTrans['amount']);
                $change = '+' . $curTrans['amount'];
            }
            if (bccomp($newBalance,$account_mins[$accountId]) == -1) $account_mins[$accountId] = $newBalance;
            if (bccomp($newBalance,$account_maxs[$accountId]) == 1) $account_maxs[$accountId] = $newBalance;
            $running_balances[$accountId] = $newBalance;
            add_to_indexed_indexed_array($account_balances_by_day,$accountId,$transDate,array(
                'date' => $transDate,
                'name' => $curTrans['name'],
                'change' => $change,
                'balance' => $newBalance,
            ));

        }
        foreach ($withdrawals as $curTrans) {
            $newBalance = $running_balances[$accountId];
            $change = '0.00';
            if ($accountId == $curTrans['from']) {
                // its a withdrawal
                $newBalance = bcsub($newBalance,$curTrans['amount']);
                $change = '-' . $curTrans['amount'];
            } else if ($accountId == $curTrans['to']) {
                // it's a deposit
                $newBalance = bcadd($newBalance,$curTrans['amount']);
                $change = '+' . $curTrans['amount'];
            }
            if (bccomp($newBalance,$account_mins[$accountId]) == -1) $account_mins[$accountId] = $newBalance;
            if (bccomp($newBalance,$account_maxs[$accountId]) == 1) $account_maxs[$accountId] = $newBalance;
            $running_balances[$accountId] = $newBalance;
            add_to_indexed_indexed_array($account_balances_by_day,$accountId,$transDate,array(
                'date' => $transDate,
                'name' => $curTrans['name'],
                'change' => $change,
                'balance' => $newBalance,
            ));
        }
    }
}

// print all the data to the page
foreach ($accounts as $account) {
    $accountId = $account['id'];
    $min = $account_mins[$accountId];
    $max = $account_maxs[$accountId];
    print("
<details>
<summary><nav><ul><li>${account['name']} Balance</li></ul><ul><li>Min: ${min}</li><li>Max: ${max}</li></ul></nav></summary>
<table role=\"grid\">
<thead>
<tr>
<th>Date</th>
<th>Name</th>
<th>Change</th>
<th>Balance</th>
</tr>
</thead>
<tbody>
");
foreach ($account_balances_by_day[$accountId] as $acBal) {
    foreach ($acBal as $acChange) {
        $balTag = 'ins';
        $changeTag = 'ins';
        if (substr($acChange['change'],0,1) == '-') $changeTag = 'mark';
        if (substr($acChange['balance'],0,1) == '-') $balTag = 'mark';
        htmlentities_array($acChange);
        print("
<tr>
<td>${acChange['date']}</td>
<td>${acChange['name']}</td>
<td><${changeTag}>${acChange['change']}</${changeTag}></td>
<td><${balTag}>${acChange['balance']}</${balTag}></td>
</tr>
");    
    }
}
    print("
</tbody>
</table>
</details>
");
}
?>
</main>

</body>
</html>
