<?php
include '.include.php';

$returnPage = 'recurring.php';
$page = 'Recurring Transaction';

$editing = false;
$edit_data = array();

$accounts = get_accounts();

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'save') {
        $edit_data['id'] = $_POST['id'];
        $edit_data['name'] = $_POST['name'];
        $edit_data['amount'] = $_POST['amount'];
        $edit_data['every'] = $_POST['every'];
        $edit_data['every_x'] = $_POST['every_x'];
        $edit_data['from'] = $_POST['from'];
        $edit_data['to'] = $_POST['to'];
        $edit_data['startDate'] = $_POST['startDate'];
        $edit_data['endDate'] = $_POST['endDate'];
        save_recurring($edit_data);
        header('Location: '.$returnPage,true,302);
        exit;
    } elseif ($_POST['action'] == 'delete') {
        delete_recurring($_POST['id']);
        header('Location: '.$returnPage,true,302);
        exit;
    }
}
if (isset($_GET['id'])) {
    $editing = true;
    $id = $_GET['id'];
    $edit_data = load_recurring($id);
    if ($edit_data === false) $edit_data = new_recurring();
    htmlentities_array($edit_data);
} else {
    $edit_data = new_recurring();
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php echo $htmlTitle; ?></title>
<link rel="stylesheet" href="<?php echo $cssInclude; ?>" />
</head>
<body>

<main class="container-fluid">
<?php include 'navigation.html'; ?>
<h1 style="color: #ed3bbe;"><?php echo $editing ? 'Edit' : 'Add'; ?> <?php echo $page; ?></h1>

<form method="POST">
<input type="hidden" name="action" value="save" />
<input type="hidden" name="id" value="<?php echo $edit_data['id']; ?>" />
<input type="text" name="name" placeholder="Name" required value="<?php echo $edit_data['name']; ?>"/>
<input type="number" name="amount" step="0.01" placeholder="Amount" required value="<?php echo $edit_data['amount']; ?>"/>
<select name="every" required>
<?php printRecurringOptions($edit_data['every']); ?>
</select>
<input type="number" name="every_x" step="1" placeholder="X" required value="<?php echo $edit_data['every_x']; ?>"/>
From Account: <select name="from" required>
<?php printAccountOptions($accounts,$edit_data['from']); ?>
</select>
To Account: <select name="to" required>
<?php printAccountOptions($accounts,$edit_data['to']); ?>
</select>
Start Date:<input type="date" name="startDate" placeholder="Start Date" required value="<?php echo $edit_data['startDate']; ?>"/>
End Date:<input type="date" name="endDate" placeholder="End Date" value="<?php echo $edit_data['endDate']; ?>"/>
<button type="submit">Save</button>
</form>
<form action="<?php echo $returnPage ?>" method="GET">
<button type="submit" style="background-color: #666;">Cancel</button>
</form>
<form method="POST">
<input type="hidden" name="action" value="delete" />
<input type="hidden" name="id" value="<?php echo $edit_data['id']; ?>" />
<button type="submit" style="background-color: #f00;">Delete</button>
</form>

</main>

</body>
</html>
