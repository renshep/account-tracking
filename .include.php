<?php
$htmlTitle = 'Account Tracking';
$cssInclude = 'pico.classless.min.css';
$cssInclude = 'pico.min.css';
$dataDir = './data';
$accountDir = $dataDir . '/accounts';
$recurringDir = $dataDir . '/recurring';
$singleDir = $dataDir . '/single';
$reportLength = '+5 years';
function debug($s) {
    error_log(serialize($s));
}
function date_to_str($d) {
    return date_format($d,'Y-m-d');
}
function date_now() {
    return date_create('midnight today');
}
function get_earliest_account_date(&$accounts) {
    $res = date_now();
    $ea = reset($accounts);
    if ($ea === false) return $res;
    $eaDate = date_create($ea['startDate']);
    if ($eaDate === false) return $res;
    return $eaDate;
}
function id_to_filename($id) {
    return hash('sha256',$id);
}
function new_uuid() {
    return trim(`uuidgen`);
}
function save_to_file($data,$dir) {
    if (!isset($data['id'])) return false;
    $fname = id_to_filename($data['id']);
    $destFile = $dir . '/' . $fname;
    return file_put_contents($destFile,serialize($data)) !== false;
}
function load_from_file($id,$dir) {
    $fname = id_to_filename($id);
    $srcFile = $dir . '/' . $fname;
    return unserialize(file_get_contents($srcFile));
}
function delete_file($id,$dir) {
    $fname = id_to_filename($id);
    $delFile = $dir . '/' . $fname;
    return unlink($delFile);
}
function get_all($dir,$index=array('id')) {
    $res = array();
    foreach (glob($dir . '/*') as $file) {
        $new_res = unserialize(file_get_contents($file));
        $new_index = '';
        foreach ($index as $index_by) {
            $new_index .= $new_res[$index_by];
        }
        $res[$new_index] = $new_res;
    }
    ksort($res);
    return $res;
}
function save_single($data) {
    global $singleDir;
    return save_to_file($data,$singleDir);
}
function save_recurring($data) {
    global $recurringDir;
    return save_to_file($data,$recurringDir);
}
function save_account($data) {
    global $accountDir;
    return save_to_file($data,$accountDir);
}
function load_single($id) {
    global $singleDir;
    return load_from_file($id,$singleDir);
}
function load_recurring($id) {
    global $recurringDir;
    return load_from_file($id,$recurringDir);
}
function load_account($id) {
    global $accountDir;
    return load_from_file($id,$accountDir);
}
function delete_single($id) {
    global $singleDir;
    return delete_file($id,$singleDir);
}
function delete_recurring($id) {
    global $recurringDir;
    return delete_file($id,$recurringDir);
}
function delete_account($id) {
    global $accountDir;
    return delete_file($id,$accountDir);
}
function get_accounts($sort=array('startDate','name','id')) {
    global $accountDir;
    return get_all($accountDir,$sort);
}
function new_account($id=false) {
    if ($id === false) $id = new_uuid();
    return array(
        'id' => $id,
        'name' => '',
        'startDate' => '',
        'balance' => '',
    );
}
function get_single($sort=array('date','name','id')) {
    global $singleDir;
    return get_all($singleDir,$sort);
}
function get_recurring($sort=array('startDate','name','id')) {
    global $recurringDir;
    return get_all($recurringDir,$sort);
}
function new_recurring($id=false) {
    if ($id === false) $id = new_uuid();
    return array(
        'id' => $id,
        'name' => '',
        'amount' => '',
        'every' => '',
        'every_x' => '',
        'from' => '',
        'to' => '',
        'startDate' => '',
        'endDate' => '',
    );
}
function new_single($id=false) {
    if ($id === false) $id = new_uuid();
    return array(
        'id' => $id,
        'name' => '',
        'amount' => '',
        'from' => '',
        'to' => '',
        'date' => '',
    );
}
function printRecurringOptions($selectedValue='') {
    $ops = array(
        'days' => 'Every X Days',
        'weeks' => 'Every X Weeks',
        'months' => 'Every X Months',
        'years' => 'Every X Years',
    );
    foreach ($ops as $opk => $op) {
        $selected = '';
        if ($selectedValue === $opk) $selected = ' selected';
        print("<option value=\"${opk}\"${selected}>${op}</option>");
    }
}
function printAccountOptions($accounts,$selectedAccount='') {
    foreach ($accounts as $account) {
        $id = htmlentities($account['id']);
        $name = htmlentities($account['name']);
        $selected = '';
        if ($selectedAccount === $account['id']) $selected = ' selected';
        print("<option value=\"${id}\"${selected}>${name}</option>");
    }
}
function htmlentities_array(&$a) {
    foreach ($a as &$value) {
        $value = htmlentities($value);
    }
    unset($value);
}
function htmlentities_array_array(&$a) {
    foreach ($a as &$value) {
        htmlentities_array($value);
    }
    unset($value);
}
function add_to_indexed_array(&$array,$index,$value) {
    if (!isset($array[$index])) $array[$index] = array();
    $array[$index][] = $value;
}
function add_to_indexed_indexed_array(&$array,$index1,$index2,$value) {
    if (!isset($array[$index1])) $array[$index1] = array();
    if (!isset($array[$index1][$index2])) $array[$index1][$index2] = array();
    $array[$index1][$index2][] = $value;
}
function new_transaction($date,$name,$amount,$from,$to) {
    $res = array(
        'date' => $date,
        'name' => $name,
        'amount' => $amount,
        'from' => $from,
        'to' => $to,
    );
    return $res;
}

?>
