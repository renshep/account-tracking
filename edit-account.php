<?php
include '.include.php';

$returnPage = 'accounts.php';
$page = 'Account';

$editing = false;
$edit_data = array();

if (isset($_POST['action'])) {
    if ($_POST['action'] == 'save') {
        $edit_data['id'] = $_POST['id'];
        $edit_data['name'] = $_POST['name'];
        $edit_data['startDate'] = $_POST['startDate'];
        $edit_data['balance'] = $_POST['balance'];
        save_account($edit_data);
        header('Location: '.$returnPage,true,302);
        exit;
    } elseif ($_POST['action'] == 'delete') {
        delete_account($_POST['id']);
        header('Location: '.$returnPage,true,302);
        exit;
    }
}
if (isset($_GET['id'])) {
    $editing = true;
    $id = $_GET['id'];
    $edit_data = load_account($id);
    if ($edit_data === false) $edit_data = new_account();
    htmlentities_array($edit_data);
} else {
    $edit_data = new_account();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php echo $htmlTitle; ?></title>
<link rel="stylesheet" href="<?php echo $cssInclude; ?>" />
</head>
<body>

<main class="container-fluid">
<?php include 'navigation.html'; ?>
<h1 style="color: #ed3bbe;"><?php echo $editing ? 'Edit' : 'Add'; ?> <?php echo $page; ?></h1>

<form method="POST">
<input type="hidden" name="action" value="save" />
<input type="hidden" name="id" value="<?php echo $edit_data['id']; ?>" />
<input type="text" name="name" placeholder="Account Name" required value="<?php echo $edit_data['name']; ?>"/>
Start Date: <input type="date" name="startDate" placeholder="Start Date" required value="<?php echo $edit_data['startDate']; ?>"/>
<input type="number" name="balance" step="0.01" placeholder="Balance" required value="<?php echo $edit_data['balance']; ?>"/>
<button type="submit">Save</button>
</form>
<form action="<?php echo $returnPage ?>" method="GET">
<button type="submit" style="background-color: #666;">Cancel</button>
</form>
<form method="POST">
<input type="hidden" name="action" value="delete" />
<input type="hidden" name="id" value="<?php echo $edit_data['id']; ?>" />
<button type="submit" style="background-color: #f00;">Delete</button>
</form>

</main>

</body>
</html>
