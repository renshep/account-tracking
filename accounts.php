<?php
include '.include.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php echo $htmlTitle; ?></title>
<link rel="stylesheet" href="<?php echo $cssInclude; ?>" />
</head>
<body>

<main class="container-fluid">
<?php include 'navigation.html'; ?>

<h1 style="color: #ed3bbe;">Accounts</h1>
<a href="edit-account.php">Add New Account</a>
<figure>
<table role="grid">
<thead>
<tr>
<th>Name</th>
<th>Start Date</th>
<th>Balance</th>
</tr>
</thead>
<tbody>
<?php
foreach (get_accounts() as $account) {
    htmlentities_array($account);
    print("
<tr>
<td><a href=\"edit-account.php?id=${account['id']}\">${account['name']}</a></td>
<td>${account['startDate']}</td>
<td>\$ ${account['balance']}</td>
</tr>
");
}
?>
</tbody>
</table>
</figure>
</main>

</body>
</html>
